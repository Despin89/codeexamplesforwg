﻿namespace GD.UI
{
    using Core.MessengerSystem;
    using Game.ItemsSystem;

    public class InventorySlot : SlotBase
    {
        public override bool TryInsertItem(Item item)
        {
            return true;
        }

        public override void DropItemObject(ItemObject droppedItem)
        {
            this.InstantiateItemObject(droppedItem);
            Messenger<Item>.Broadcast(M.DROP_ITEM_TO_INV, this.ItemObjectInSlot.Item);
        }

        public override void InstantiateItemObject(ItemObject droppedItem)
        {
            droppedItem.transform.SetParent(this.transform);
            droppedItem.transform.position = this.transform.transform.position;

            this.ItemObjectInSlot = droppedItem;
        }

        public override void GrabItemObject()
        {
            Messenger<Item>.Broadcast(M.GRAB_ITEM_FROM_INV, this.ItemObjectInSlot.Item);
            this.ItemObjectInSlot = null;
        }
    }
}