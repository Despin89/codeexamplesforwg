﻿namespace GD.UI
{
    using Game.ItemsSystem;
    using UnityEngine.EventSystems;

    public abstract class SlotBase
        : UIBase, IDropHandler
    {

        public ItemObject ItemObjectInSlot { get; set; }
        private bool TryChangeItems(ItemObject itemToChange)
        {
            return this.TryInsertItem(itemToChange.Item) &&
                   ItemObject.StartDragSlot.TryInsertItem(this.ItemObjectInSlot.Item);
        }

        public abstract bool TryInsertItem(Item item);

        public abstract void DropItemObject(ItemObject droppedItem);

        public abstract void InstantiateItemObject(ItemObject droppedItem);

        public abstract void GrabItemObject();

        public void OnDrop(PointerEventData eventData)
        {
            if (!ItemObject.StartDragSlot.Equals(this))
            {
                if (this.ItemObjectInSlot == null)
                {
                    if (this.TryInsertItem(ItemObject.DraggedItemObject.Item))
                    {
                        this.DropItemObject(ItemObject.DraggedItemObject);
                        ItemObject.StartDragSlot.GrabItemObject();

                        ItemObject.DragComplete();
                    }
                }
                else
                {
                    if (this.TryChangeItems(ItemObject.DraggedItemObject))
                    {
                        ItemObject tempItem = this.ItemObjectInSlot;

                        this.GrabItemObject();
                        this.DropItemObject(ItemObject.DraggedItemObject);
                        ItemObject.StartDragSlot.DropItemObject(tempItem);

                        ItemObject.DragComplete();
                    }
                }
            }
        }
    }
}