﻿namespace GD.UI
{
    using System;
    using Extentions;
    using Game.ItemsSystem;
    using PopupWindows;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class ItemObject
        : UIBase,
            IBeginDragHandler,
            IDragHandler,
            IEndDragHandler,
            IPointerEnterHandler,
            IPointerExitHandler,
            IPointerClickHandler,
            IDisposable
    {

        private static Transform _parentForDrag;

        private static bool _isDragging;

        private int _itemsAmount;

        private static GameObject _itemObjectPrfb;

        public static ItemObject DraggedItemObject { get; private set; }

        public static SlotBase StartDragSlot { get; private set; }

        public Sprite ImageSprite
        {
            private set { this.GetComponent<Image>().sprite = value; }
            get { return this.GetComponent<Image>().sprite; }
        }

        public Item Item { get; private set; }

        public int ItemsAmount
        {
            get { return this._itemsAmount; }
            set
            {
                if (value <= 0)
                    throw new Exception("ItemsAmount of ItemObject cant be less or equal to 0.");

                this._itemsAmount = value;
                this.transform.GetTextComponent().text = this._itemsAmount == 1
                    ? string.Empty
                    : this._itemsAmount.ToString();
            }
        }

        private static GameObject ItemObjectPrfb
        {
            get
            {
                return _itemObjectPrfb ?? (_itemObjectPrfb = Resources.Load("UI/Inventory/ItemObject") as GameObject);
            }
        }

        public static void DragComplete()
        {
            _isDragging = false;
            Reset();
        }

        public static ItemObject Instantiate(Item item, int amount = 1)
        {
            ItemObject itemObject = Instantiate(ItemObjectPrfb).GetComponent<ItemObject>();

            itemObject.Item = item;
            itemObject.ImageSprite = item.ItemSprite;
            itemObject.ItemsAmount = amount;

            return itemObject;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            GenericTooltip.ShowTooltip(this.Item, null, this.transform.position);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            GenericTooltip.Deactivate();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            StartDragSlot = this.transform.parent.gameObject.GetComponent<SlotBase>();
            DraggedItemObject = this;

            if ((StartDragSlot == null) || (this.Item == null))
            {
                throw new Exception("StartDragSlot or Item is NULL.");
            }

            _isDragging = true;

            this.transform.SetParent(_parentForDrag);
            this.transform.position = eventData.position;
            this.transform.localScale *= 1.2F;

            this.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if ((StartDragSlot == null) || !_isDragging)
                throw new Exception("OnDrag invoke without OnBeginDrag.");

            this.transform.position = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_isDragging)
            {
                this.transform.SetParent(StartDragSlot.transform);
                this.transform.position = StartDragSlot.transform.position;
                Reset();
            }

            this.transform.localScale = Vector3.one;
            this.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                PropertiesMenu.Instance.ShowMenu(transform.position);
            }
        }

        private static void Reset()
        {
            StartDragSlot = null;
            DraggedItemObject = null;
        }

        private void Start()
        {
            if (_parentForDrag == null)
            {
                _parentForDrag = GameObject.Find("InfoPanel").GetComponent<Transform>();
            }
        }

        public void Dispose()
        {
            this.Item = null;
            this.Destroy();
        }
    }
}