﻿namespace GD.UI
{
    using System.Collections.Generic;
    using System.Linq;
    using Core.MessengerSystem;
    using Game;
    using Game.ItemsSystem;
    using PopupWindows;
    using UnityEngine;

    public class InventoryUI : MonoBehaviour, IWorld
    {
        private List<InventorySlot> Slots { get; set; }

        private const int SLOT_AMOUNT = 20;

        private const int MAX_STACK_AMOUNT = 10;

        [SerializeField]
        private GameObject _slotInventoryPrefab;

        public void Init()
        {
            Messenger<Item>.AddListener(M.ADD_ITEM_TO_INV, this.AddItemToInv);

            this.Slots = new List<InventorySlot>();

            for (int i = 0; i < SLOT_AMOUNT; i++)
            {
                GameObject slotObj = Instantiate(this._slotInventoryPrefab);
                slotObj.transform.SetParent(this.gameObject.transform);

                InventorySlot slotInv = slotObj.GetComponent<InventorySlot>();
                this.Slots.Add(slotInv);
            }

            this.InstantiateInventoryItems();
        }

        private void InstantiateInventoryItems()
        {
            foreach (Item item in this.CurrentWorld.Inventory.PartyItemsBag)
            {
                if (item.Stackable)
                {
                    bool done = false;
                    foreach (
                        InventorySlot s in
                        this.Slots.Where(s => s.ItemObjectInSlot != null)
                            .Where(
                                   s => s.ItemObjectInSlot.ItemsAmount < MAX_STACK_AMOUNT)
                    )
                    {
                        s.ItemObjectInSlot.ItemsAmount++;
                        done = true;
                        break;
                    }

                    if (done)
                    {
                        continue;
                    }
                }

                InventorySlot slot = this.Slots.FirstOrDefault(e => e.ItemObjectInSlot == null);
                if (slot == null)
                {
                    ModalDialogWindow.Instance.InvokeModalMessage("Here is no empty slot!");
                    return;
                }

                slot.InstantiateItemObject(ItemObject.Instantiate(item));
            }
        }

        private void AddItemToInv(Item item)
        {
            //Если есть такой же, стакающийся предмет
            if (item.Stackable)
            {
                foreach (
                    InventorySlot s in
                    this.Slots.Where(s => s.ItemObjectInSlot != null)
                        .Where(
                               s => s.ItemObjectInSlot.ItemsAmount < MAX_STACK_AMOUNT)
                )
                {
                    s.ItemObjectInSlot.ItemsAmount++;
                    return;
                }
            }

            //Если есть место для предмета
            foreach (InventorySlot slot in this.Slots.Where(slot => slot.ItemObjectInSlot == null))
            {
                slot.DropItemObject(ItemObject.Instantiate(item));
                return;
            }

            ModalDialogWindow.Instance.InvokeModalMessage("Here is no empty slot!");
        }

        private void OnDestroy()
        {
            Messenger<Item>.RemoveListener(M.ADD_ITEM_TO_INV, this.AddItemToInv);
        }

        public World CurrentWorld
        {
            get { return GameManager.CurrentWorld; }
        }
    }
}