﻿using System.Collections.Generic;

namespace GD.Game.ItemsSystem
{
    using System.Linq;
    using ActionsSystem;
    using UI.PopupWindows;
    using UnityEngine;

    public sealed class Item : ITooltip
    {
        private bool _stackable;

        private int _value;

        private string _subtype;

        private ItemType _type;

        private List<ComponentBase> _components;

        private string _name;

        private string _tooltip;

        private Sprite _itemSprite;

        private string _tag;

        public Item(ItemInfo info)
        {
            this._stackable = info.Stackable;
            this._value = info.Value;
            this._subtype = info.Subtype?.Name ?? "None";
            this._type = info.Type;
            this._name = info.Name.Text;
            this._tooltip = info.Tooltip.Text;
            this._itemSprite = info.ItemSprite;
            this._tag = info.Tag;

            this._components = info.ComponentInfos.Select(_ => _.GetInstance()).ToList();
        }

        public bool IsEquipable
        {
            get { return (this._type != ItemType.Consumable) && (this._type != ItemType.None); }
        }

        public bool Stackable
        {
            get { return this._stackable; }
        }

        public int Value
        {
            get { return this._value; }
        }

        public string Subtype
        {
            get { return this._subtype; }
        }

        public List<ComponentBase> Components
        {
            get { return this._components; }
        }

        public string Name
        {
            get { return this._name; }
        }

        public string Tooltip
        {
            get { return this._tooltip; }
        }

        public Sprite ItemSprite
        {
            get { return this._itemSprite; }
        }

        public string Tag
        {
            get { return this._tag; }
        }

        public ItemType Type
        {
            get { return this._type; }
        }

        public string GenerateTooltip()
        {
            return this._tooltip;
        }
    }
}
