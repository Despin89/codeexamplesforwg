﻿namespace GD.Game.ItemsSystem
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Items/ItemSubtype", fileName = "ItemSubtype")]
    public class ItemSubtype : AssetEnumBase
    {
    }
}