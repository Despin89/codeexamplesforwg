﻿namespace GD.Extentions
{
    using System;
    using System.Collections;
    using Game;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using Object = UnityEngine.Object;

    public static class UnityExtentions
    {
        #region Публичные методы

        public static bool V3Eq_0001(this Vector3 a, Vector3 b)
        {
            return Vector3.SqrMagnitude(a - b) < 0.0001;
        }

        public static void SetLayer(this GameObject parent, string layer, bool includeChildren = true)
        {
            int layerNumber = LayerMask.NameToLayer(layer);
            parent.layer = layerNumber;
            if (includeChildren)
            {
                foreach (Transform trans in parent.transform.GetComponentsInChildren<Transform>(true))
                {
                    trans.gameObject.layer = layerNumber;
                }
            }
        }

        public static void SetLayer(this MonoBehaviour parent, string layer, bool includeChildren = true)
        {
            int layerNumber = LayerMask.NameToLayer(layer);
            parent.gameObject.layer = layerNumber;
            if (includeChildren)
            {
                foreach (Transform trans in parent.gameObject.transform.GetComponentsInChildren<Transform>(true))
                {
                    trans.gameObject.layer = layerNumber;
                }
            }
        }

        public static T GetComponentInChild<T>(this MonoBehaviour o, string childName)
        {
            return o.transform.Find(childName).GetComponent<T>();
        }

        public static GameObject GetChild(this MonoBehaviour o, string childName)
        {
            return o.transform.Find(childName).gameObject;
        }

        public static void DestroyAllChildren(this MonoBehaviour o)
        {
            foreach (Transform child in o.gameObject.transform)
            {
                child.gameObject.Destroy();
            }
        }

        public static void DestroyAllChildren(this GameObject o)
        {
            foreach (Transform child in o.transform)
            {
                child.gameObject.Destroy();
            }
        }

        public static void DestroyAllChildren(this Transform t)
        {
            foreach (Transform child in t)
            {
                child.gameObject.Destroy();
            }
        }

        public static void DelayedAction(float waitTime, Action action, Func<bool> allower = null)
        {
            GameManager.Instance.StartCoroutine(DelayedActionCoroutine(waitTime, action, allower));
        }

        private static IEnumerator DelayedActionCoroutine(float waitTime, Action action, Func<bool> allower)
        {
            yield return new WaitForSeconds(waitTime);

            if (allower != null)
            {
                if (allower())
                    action?.Invoke();
            }
            else
                action?.Invoke();
        }

        public static IEnumerator Wait(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
        }

        public static void ActivateSelectable(this Selectable selectable)
        {
            selectable.interactable = true;
        }

        public static void DeactivateSelectable(this Selectable selectable)
        {
            selectable.interactable = false;
        }

        public static void ActivateGO(this GameObject gObj)
        {
            gObj.SetActive(true);
        }

        public static void SetLastAndActivate(this GameObject gObj)
        {
            gObj.ActivateGO();
            gObj.transform.SetAsLastSibling();
        }

        public static void DeactivateGO(this GameObject gObj)
        {
            gObj.SetActive(false);
        }

        public static void Destroy(this GameObject gObj)
        {
            Object.Destroy(gObj);
        }

        public static void Destroy(this MonoBehaviour mb)
        {
            mb.gameObject.Destroy();
        }

        public static void AddAction(this Button button, UnityAction action)
        {
            button.onClick.AddListener(action);
        }

        public static Text Text(this Button button)
        {
            return button.transform.GetChild(0).GetComponent<Text>();
        }

        public static T GetChildComponent<T>(this Transform transform)
        {
            T result = transform.GetComponentInChildren<T>();
            if (result == null)
            {
                throw new Exception("Cannot find a component of type: {0}".F(typeof(T)));
            }
            return result;
        }

        public static Text GetTextComponent(this Transform transform)
        {
            Text result = transform.GetComponentInChildren<Text>();
            if (result == null)
            {
                throw new Exception("Cannot find a component with a Name: Text");
            }
            return result;
        }

        public static Vector3 WorldToScreen(this MonoBehaviour mb, Vector3 pos)
        {
            return Camera.main.WorldToScreenPoint(pos);
        }

        #endregion
    }
}