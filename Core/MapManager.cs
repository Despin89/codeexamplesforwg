﻿namespace GD.Game.MapSystem
{
    using System.Collections.Generic;
    using System.Linq;
    using Core;
    using Core.MessengerSystem;
    using Extentions;
    using UnityEngine;

    public class MapManager : Singleton<MapManager>
    {
        #region Constants

        private const float POS_MULT = 1F;

        private const string LEVEL_TYPES = "Data/EncountersDatabase/LevelTypes.xml";

        private const string LEVEL_DISCRIPT = "Data/EncountersDatabase/LevelDescriptions.xml";

        #endregion

        #region Fields

        [SerializeField]
        private GameObject _backPrefab;

        [SerializeField]
        private GameObject _corridorTilePrefab;

        [SerializeField]
        private GameObject _roomTilePrefab;

        private LevelDescription _levelDescription;

        private List<TileBase> _tiles;

        #endregion

        public void DrawMap()
        {
            Transform mapParent = GameObject.FindGameObjectWithTag("MapParent").transform;
            mapParent.DestroyAllChildren();

            int[,] map = MapGen.GenerateMap();

            this.SetDescription(MapGen.TrueRoomsCount);

            this._tiles = new List<TileBase>();

            float offset = (float)MapGen.MAP_SIDE_AS_TILES / 2;

            for (int x = 0; x < MapGen.MAP_SIDE_AS_TILES; x++)
            for (int y = 0; y < MapGen.MAP_SIDE_AS_TILES; y++)
            {
                if (map[x, y] != MapGen.WALL)
                {
                    TileBase tile;

                    if (map[x, y] == MapGen.ROOM)
                    {
                        tile = Instantiate(this._roomTilePrefab,
                                           new Vector2(x * POS_MULT - offset, -y * POS_MULT + offset),
                                           Quaternion.identity)
                            .GetComponent<TileBase>();
                        tile.name = "RoomTile." + x + "." + y;
                        tile.EncounterName = this._levelDescription.GetEncounter();
                    }
                    else
                    {
                        tile = Instantiate(this._corridorTilePrefab,
                                           new Vector2(x * POS_MULT - offset, -y * POS_MULT + offset),
                                           Quaternion.identity)
                            .GetComponent<TileBase>();
                        tile.name = "CorridorTile." + x + "." + y;
                    }

                    tile.GridPosition = new Vector2(x, y);
                    tile.transform.SetParent(mapParent, true);

                    this._tiles.Add(tile);
                }
            }

            Messenger<List<TileBase>>.Broadcast(M.MAP_LOADED, this._tiles);
        }

        private void SetDescription(int rooms)
        {
            List<LevelType> levelTypes = Ex.ToListFromXML<LevelType>(LEVEL_TYPES);
            List<LevelDescription> levelDescriptions = Ex.ToListFromXML<LevelDescription>(LEVEL_DISCRIPT);

            string level = levelTypes.GetRandElement().Levels.GetRandElement();
            this._levelDescription = levelDescriptions.First(e => e.Name == level);
            this._levelDescription.Init(rooms);
        }
    }
}